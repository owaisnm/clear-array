% Owais Naeem
% Clear Array Project

arrSize = [100 1000 1500 10000 50000 100000];
unoptIndices = [1.71 11.97 15.39 150.52 552.48 1030.84];
unoptPointers = [1.71 13.11 13.68 93.51 466.96 1010.31];
optIndices = [1.14 4.56 9.69 41.05 250.30 579.28];
optPointers = [1.14 3.42 4.56 26.23 150.52 276.52];

plot(arrSize, unoptIndices, ':', 'LineWidth', 3);
hold on
plot(arrSize, unoptPointers,'-.', 'LineWidth', 3);
plot(arrSize, optIndices,'-', 'LineWidth', 3);
plot(arrSize, optPointers,'--', 'LineWidth', 3);
hold off;
grid
xlabel('Array Sizes (# of elements)');
ylabel('Time (microseconds)');
title('Unoptimized vs Optimized Clear Array')
legend('unoptimized indices', 'unoptimized pointers', 'optimized indices', 'optimized pointers');