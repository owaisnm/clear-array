#include <iostream>
#include <tchar.h>
#include <Windows.h>
#include "clear.h"

using namespace std;

int main()
{
	int ARRAY_SIZE[6] = {100, 1000, 1500, 10000, 50000, 100000};
	int arr1[100];
	int arr2[1000];
	int arr3[1500];
	int arr4[10000];
	int arr5[50000];
	int arr6[100000];

	__int64 ctr1 = 0, ctr2 = 0, freq = 0;

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr1, ARRAY_SIZE[0]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[0] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr1, ARRAY_SIZE[0]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[0] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr2, ARRAY_SIZE[1]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[1] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr2, ARRAY_SIZE[1]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[1] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr3, ARRAY_SIZE[2]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[2] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr3, ARRAY_SIZE[2]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[2] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr4, ARRAY_SIZE[3]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[3] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr4, ARRAY_SIZE[3]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[3] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr5, ARRAY_SIZE[4]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[4] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr5, ARRAY_SIZE[4]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[4] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for pointers
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_pointers(arr6, ARRAY_SIZE[5]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "POINTERS" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[5] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	// start timing the code for indices
	if(QueryPerformanceCounter((LARGE_INTEGER *)&ctr1) != 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr1);
		clear_indices(arr6, ARRAY_SIZE[5]);
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr2);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		cout << "----------------------------------------" << endl;
		cout << "INDICES" << endl;
		cout << "array size: ";
		cout << ARRAY_SIZE[5] << endl;
		cout << "Time Elapsed (microseconds): ";
		cout << (((double)(ctr2 - ctr1))/freq)*1000000 << endl; // in microseconds
		ctr1 = 0; ctr2 = 0; freq = 0;
	}

	cout << "----------------------------------------" << endl;
	cin.get();

	return 0;
}

