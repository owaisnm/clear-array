#include "clear.h"

void clear_indices(int arr[], int size) 
{
	int i;
	for(i = 0; i < size; i = i + 1)
		arr[i] = 0;
}

void clear_pointers(int arr[], int size)
{
	int *p;
	for(p = &arr[0]; p < &arr[size]; p = p + 1)
		*p = 0;
}

